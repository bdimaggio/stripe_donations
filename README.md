# Code Exercise for ThinkShout Drupal Architect Position

## Installation
Prerequsite for use of Stripe's live environment: a live server for this site that forces HTTPS.
1. Download, `composer install`, and install this Drupal project, adapted from [the Drupal Composer template](https://github.com/drupal-composer/drupal-project). Be sure to choose a "standard" installation.
2. Enable the stripe_donations module, either with Drush or via the GUI.

## What to look for
* The donation page, at /donate
* Donation settings, at /admin/config/services/donation-settings
* The list of donations so far, at /admin/content/donation/list

## Reflections on the work
### Approach
I started by reading over the Stripe API documentation. I wanted to be sure to base my architectural choices on the functions they provided. There weren't any big curve balls (in fact, I really liked the docs--see below), but, e.g., knowing that the card element was going to need to be dropped in via JavaScript helped me plan ahead to have that library available when I built the donation form.

One issue I spent a fair amount of time thinking about was how to store donation information on the Drupal side. It's supposed to be simple information, per the spec, and in cases like that I am inclined to avoid the overhead of creating a custom entity in favor of a simple database table. However, I reasoned that it's likely that the client will want to expand this functionality at some point in the future--perhaps by adding fields to the donation form/donations themselves, or by requesting a "donation detail" page where they could see everything Stripe knows about a given donation--so I opted for a very simple entity.

### Working with Stripe
I wasn't wild about Stripe's ["Quickstart"-suggested JavaScript](https://stripe.com/docs/stripe-js/elements/quickstart#setup) for including the card element, but it was easy enough to clean up. The PHP API itself was great--consistent, clear, and well-documented.

### Issues
The biggest issue I faced was how to keep the installation procedure to two steps. My initial understanding was that I should provide only the stripe_donations module, but getting the Stripe PHP library into the core /vendor directory in that situation, without resorting to an additional `composer require stripe/stripe-php` step, was impossible. I reread the exercise instructions more carefully and determined that "Use a simple Drupal 8 as a starting point" meant that I should add this entire site to my repository. However, because it seemed more in keeping with current best development practices, I _didn't_ hack core's .gitignore file so that /vendor, /web/core, etc., were in the repo. That means that the "Install Drupal" step includes the sub-steps of downloading, running `composer install`, and installing. I hope that was the correct reading.

### Next steps
* Include a suite of tests.
* Email donors upon successful donation.
* Test more failure cases (where Stripe tries the transaction and it fails, but there is reason to believe that trying it subsequent times might work), and handle those situations gracefully.
* Provide the option to download a csv of Donations (perhaps in a date or Donation ID range) to site editors.
* Tie donor email addresses to Customers from Stripe (would have to use email address for this), making it possible to track a single donor whether they have given via the website or purchased things at events.

## Maintainer
Ben Di Maggio

<?php

namespace Drupal\stripe_donations\Controller;

use Drupal\Core\Controller\ControllerBase;

class StripeDonationsController extends ControllerBase {

  /**
   * Provides content for the post-donation page.
   *
   * @return array
   *  The render array.
   */
  public function thanksPage() {
    return [
      '#markup' => '<p>Thank you for your generous donation!</p>',
    ];
  }
}

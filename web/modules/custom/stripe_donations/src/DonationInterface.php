<?php

namespace Drupal\stripe_donations;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a Donation entity.
 */
interface DonationInterface extends ContentEntityInterface, EntityChangedInterface {

}

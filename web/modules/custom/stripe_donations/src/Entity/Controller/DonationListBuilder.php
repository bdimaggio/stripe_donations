<?php

namespace Drupal\stripe_donations\Entity\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
* Provides a list controller for stripe_donations_donation entity.
*/
class DonationListBuilder extends EntityListBuilder {

  /**
   * The url generator.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity.manager')->getStorage($entity_type->id()),
      $container->get('url_generator')
    );
  }

  /**
   * Constructs a new DonationListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   The url generator.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, UrlGeneratorInterface $url_generator) {
    parent::__construct($entity_type, $storage);
    $this->urlGenerator = $url_generator;
  }

  /**
   * {@inheritdoc}
   *
   * Building the header for the donation list.
   */
  public function buildHeader() {
    $header['id'] = $this->t('Donation ID');
    $header['name'] = $this->t('Name');
    $header['email'] = $this->t('Email');
    $header['amount'] = $this->t('Amount');
    $header['status'] = $this->t('Status');
    $header['changed'] = $this->t('Last updated');
    return $header;
  }

  /**
   * {@inheritdoc}
   *
   * Building each row of the donation list.
   */
  public function buildRow(EntityInterface $entity) {
    $row['id'] = $entity->id();
    $row['name'] = $entity->label();
    $row['email'] = $entity->email->value;
    $row['amount'] = '$' . sprintf("%01.2f", $entity->amount->value/100);
    $row['status'] = $entity->status->value;
    $row['changed'] = date('n/j/Y, g:i a', $entity->changed->value);
    return $row;
  }

}

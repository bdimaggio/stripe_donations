<?php

namespace Drupal\stripe_donations\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\stripe_donations\Entity\Donation;

class DonateForm extends FormBase {

  public function getFormId() {
    return 'stripe_donations_donate';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    // Establish an id element for Stripe's js to grab onto.
    $form['#attributes']['id'] = 'donation-form';

    // Get our Stripe key.
    $publishable_key = $this->getKey('publishable');

    // Grab the Stripe js library and our custom code that uses it.
    $form['#attached']['library'][] = 'stripe_donations/stripejs';
    $form['#attached']['library'][] = 'stripe_donations/donate_form';
    $form['#attached']['drupalSettings']['stripeDonations']['keys']['publishableKey'] = $publishable_key;

    $form['first_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('First name'),
      '#required' => TRUE,
      '#default_value' => '', // TODO: populate in case of failure.
    ];

    $form['last_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Last name'),
      '#required' => TRUE,
      '#default_value' => '', // TODO: populate in case of failure.
    ];

    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#required' => TRUE,
      '#default_value' => '', // TODO: populate in case of failure.
    ];

    $card_label = $this->t('Credit card information');
    $form['card'] = [
      '#prefix' => '<div class="form-wrapper"><div class="form-item"><span class="label">' . $card_label . '</span><div id="card-element">',
      '#suffix' => '</div><!-- /.form-item --><div id="card-errors" class="ajax-error"></div><!-- /.card-errors --></div><!-- /.form-wrapper -->',
    ];

    $form['amount_radios'] = [
      '#type' => 'radios',
      '#title' => $this->t('Donation amount'),
      '#options' => [
        500 => '$5.00',
        1000 => '$10.00',
        2500 => '$25.00',
        5000 => '$50.00',
        'other' => 'Other',
      ],
    ];

    $form['amount_other'] = [
      '#type' => 'number',
      '#prefix' => '<div id="other-amount-wrapper">$',
      '#suffix' => '</div>',
      '#step' => '.01',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    $form['stripe_token'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => 'stripe-token',
      ],
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    // First, the little matter of the bill.
    $amount = $form_state->getValue('amount_radios');
    if ($amount == 'other' && $amount_other = $form_state->getValue('amount_other')) {
      // We ought to be able to rely on the HTML number field to keep this
      // number sane (no dollar signs, commas, etc.)
      $amount = $amount_other * 100;
    }

    // Create our pending charge.
    $donation = Donation::create([
      'first_name' => $form_state->getValue('first_name'),
      'last_name' => $form_state->getValue('last_name'),
      'email' => $form_state->getValue('email'),
      'amount' => $amount,
      'status' => 'pending',
    ]);
    $donation->save();

    // Prepare to connect to Stripe.
    $secret_key = $this->getKey('secret');
    \Stripe\Stripe::setApiKey($secret_key);

    // Charge!!
    $charge = \Stripe\Charge::create([
      'amount' => $amount,
      'currency' => 'usd',
      'source' => $form_state->getValue('stripe_token'),
      'description' => $this->t('Donation of $@amt to Awesome Org.', ['@amt' => $form_state->getValue('amount')]),
      'statement_descriptor' => 'Awesome Org. donation', // 21 characters, just under the limit!
    ]);

    // We're gonna have to send people *somewhere* after this. Prepare for the
    // worst.
    $url = \Drupal\Core\Url::fromRoute('stripe_donations.donate');
    $msg = $this->t('Unfortunately, there was a problem charging your card. Please check the card information, or enter a different card, and try again.');

    // Update our donation with the new status, and ID, if we got those.
    if (!empty($charge->status)) {
      $donation->status->value = $charge->status;

      // While we're at it, set the redirect URL and clear out the message.
      if ($charge->status == 'succeeded') {
        $url = \Drupal\Core\Url::fromRoute('stripe_donations.thank_you');
        $msg = '';
      }
    }
    if (!empty($charge->id)) {
      $donation->stripe_id->value = $charge->id;
    }
    $donation->save();

    // Wrap up and redirect people.
    if ($msg) {
      drupal_set_message($msg, 'error');
    }
    return $form_state->setRedirectUrl($url);
  }

  /**
   * Returns whichever key you're looking for, from the environment that has
   * been chosen in Stripe Donations settings.
   *
   * @param $key
   *  Expecting either "publishable" or "secret".
   *
   * @return mixed
   *  The value of the key in question, or else false.
   */
  protected function getKey($key) {
    $stripe_config = \Drupal::config('stripe_donations.settings')->get();
    if (empty($stripe_config[$stripe_config['api_mode'] . '_' . $key . '_key'])) {
      return FALSE;
    }
    return $stripe_config[$stripe_config['api_mode'] . '_' . $key . '_key'];
  }

}

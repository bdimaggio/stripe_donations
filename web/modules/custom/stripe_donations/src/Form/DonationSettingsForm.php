<?php

namespace Drupal\stripe_donations\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class DonationSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'stripe_donations.settings',
    ];
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'stripe_donations_donation_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('stripe_donations.settings');

    $form['api_mode'] = [
      '#type'           => 'radios',
      '#title'          => $this->t('Mode'),
      '#description'    => $this->t('Determines which set of creds below to use, and therefore which Stripe environment to intreract with.'),
      '#default_value'  => $config->get('api_mode'),
      '#options'        => [
        'test' => $this->t('test'),
        'live' => $this->t('live'),
      ],
    ];

    $form['test_keys'] = [
      '#type'           => 'details',
      '#title'          => $this->t('Test key values'),
      '#open'           => TRUE,
    ];

    $form['test_keys']['test_publishable_key'] = [
      '#type'           => 'textfield',
      '#title'          => $this->t('Test publishable key'),
      '#default_value'  => $config->get('test_publishable_key'),
    ];

    $form['test_keys']['test_secret_key'] = [
      '#type'           => 'password',
      '#title'          => $this->t('Test secret key'),
      '#description'    => $config->get('test_secret_key') ? $this->t('A test secret key has been stored. Only enter a value here if you wish to overwrite the existing one.') : '',
    ];

    // Keep this one closed, but handy for the future.
    $form['live_keys'] = [
      '#type'           => 'details',
      '#title'          => $this->t('Live key values'),
    ];

    $form['live_keys']['live_publishable_key'] = [
      '#type'           => 'textfield',
      '#title'          => $this->t('Live publishable key'),
      '#default_value'  => $config->get('live_publishable_key'),
    ];

    $form['live_keys']['live_secret_key'] = [
      '#type'           => 'password',
      '#title'          => $this->t('Live secret key'),
      '#description'    => $config->get('live_secret_key') ? $this->t('A live secret key has been stored. Only enter a value here if you wish to overwrite the existing one.') : '',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // TODO: one day, when we're getting fancy, ask Stripe if incoming values
    // are valid.
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Only overwrite existing secret keys if new ones have been provided.
    foreach (['test', 'live'] as $env) {
      if ($value = $form_state->getValue("{$env}_secret_key")) {
        $this->config('stripe_donations.settings')
          ->set("{$env}_secret_key", $value)
          ->save();
      }
    }

    $this->config('stripe_donations.settings')
      ->set('api_mode', $form_state->getValue('api_mode'))
      ->set('test_publishable_key', $form_state->getValue('test_publishable_key'))
      ->set('live_publishable_key', $form_state->getValue('live_publishable_key'))
      ->save();
  }

}

(function ($, Drupal) {
  Drupal.behaviors.stripeDonations = {
    attach: function (context, settings) {
      // Set up a var we'll be using a lot.
      var $form = $('#donation-form');

      // First, get Stripe card field and set us up to receive a token.
      // Only proceed if we can definitely get the publishable key.
      if (drupalSettings.hasOwnProperty('stripeDonations') &&
        drupalSettings.stripeDonations.hasOwnProperty('keys') &&
        drupalSettings.stripeDonations.keys.hasOwnProperty('publishableKey')) {

        var stripe = Stripe(drupalSettings.stripeDonations.keys.publishableKey),
          elements = stripe.elements(),
          card = elements.create('card', {}),
          $cardErrors= $('#card-errors', $form),
          $stripeToken = $('#stripe-token', $form);

        card.mount('#card-element');

        // Leaving this in vanilla js,rather than switching to $().change(),
        // because card belongs to Stripe's js library, not jQuery.
        card.addEventListener('change', function(event) {
          if (event.error) {
            $cardErrors.html(event.error.message);
            $cardErrors.show();
          } else {
            $cardErrors.hide();
            $cardErrors.html('');
          }
        });

        // On form submission, go get a token from Stripe.
        $form.submit(function(event) {
          event.preventDefault();
          stripe.createToken(card).then(function(result) {
            if (result.error) {
              // Inform the user if there was an error.
              $cardErrors.html(result.error.message);
            } else {
              // Send the token to the server.
              $stripeToken.val(result.token.id);
              // Unbind this submit handler and actually submit.
              $form.off('submit').submit();
            }
          });
        });
      }
      else if (window.console) {
        console.log('Not able to find drupalSettings.stripeDonations.keys.publishableKey!');
      }

      // Manage radios-or-other behavior for the amount field.
      var $amountRadios = $('input[name=amount_radios]', $form),
        $otherAmountWrapper = $('#other-amount-wrapper', $form);

      $amountRadios.change(function() {
        if ($(this).filter(':checked').val() === 'other') {
          $otherAmountWrapper.show();
        }
        else {
          $otherAmountWrapper.hide();
        }
      });
    }
  };
})(jQuery, Drupal);

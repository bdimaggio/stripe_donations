# Introduction
Provides a simple donation page with several pre-configured amount options (and one freetext amount field), as well a secure credit-card entry field. Successful form submissions are sent to the Stripe payment service and logged as Donation entities, which can be seen at /admin/content/donations/list.
